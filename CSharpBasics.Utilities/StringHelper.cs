﻿using System;
using System.Text;

namespace CSharpBasics.Utilities
{
	public class StringHelper
	{
		/// <summary>
		/// Вычисляет среднюю длину слова в переданной строке без учётов знаков препинания и специальных символов
		/// </summary>
		/// <param name="inputString">Исходная строка</param>
		/// <returns>Средняя длина слова</returns>
		public int GetAverageWordLength(string inputString)
		{
			if (inputString!=null)
            {
				string[] collectionOfWords = inputString.Split(' '); // Разделяем слова на подстроки.
				int[] nubmberOfLetters = СountingСharacters(collectionOfWords);
				int sumOfWords = 0;
				int wordCount = 0;
				for (int charIndex = 0; charIndex < nubmberOfLetters.Length; charIndex++)
				{
					if (nubmberOfLetters[charIndex] > 0)
					//Проверяем были ли буквы в подстроке, если да, то считаем ее за слово.
					{
						wordCount += 1;
					}
					sumOfWords += nubmberOfLetters[charIndex]; // Считаем общее количество букв.
				}
				int answer = 0;

				if (wordCount > 0)
				// Проверяем были ли слова в полученной строке, если да, то получаем ответ.
				{
					answer = (sumOfWords / wordCount);
				}
				return answer;
			}
            else
            {
				return 0;
            }
		}
		private int[] СountingСharacters(string[] collectionOfWords)
        {
			int[] nubmberOfLetters = new int[collectionOfWords.Length];
			for (int word = 0; word < collectionOfWords.Length; word++)
			{
				for (int indexSimvola = 0; indexSimvola < collectionOfWords[word].Length; indexSimvola++)
				{
					if (char.IsLetter(collectionOfWords[word][indexSimvola]))
					// Проверяем является ли текущий символ буквой и если да, то прибавляем к соответствующему элементу массива 1.
					{
						nubmberOfLetters[word] += 1;
					}
				}
			}
			return nubmberOfLetters;
		}

		/// <summary>
		/// Удваивает в строке <see cref="original"/> все буквы, принадлежащие строке <see cref="toDuplicate"/>
		/// </summary>
		/// <param name="original">Строка, символы в которой нужно удвоить</param>
		/// <param name="toDuplicate">Строка, символы из которой нужно удвоить</param>
		/// <returns>Строка <see cref="original"/> с удвоенными символами, которые есть в строке <see cref="toDuplicate"/></returns>
		public string DuplicateCharsInString(string original, string toDuplicate)
		{
			if (string.IsNullOrEmpty(original) || string.IsNullOrEmpty(toDuplicate))
			// Проверяем были ли переданы корректные строки, если нет, то возвращаем текущий вариант строки.
			{
				return original;
            }
			StringBuilder collectionOfSymbols = new StringBuilder("");
			while (toDuplicate.Length > 0)
			{
				collectionOfSymbols.Append(toDuplicate[0].ToString());
				if (char.IsLetter(toDuplicate[0]))
				// Проверяем является ли текущий символ буквой.
				{
					// Если теущий символ  принадлежит к нижнему регистру,то добавляем в массив этот символ с верхним регистром.
					// Если теущий символ  принадлежит к верхнему регистру,то добавляем в массив этот символ с нижним регистром.
					AddDifferentRegister(toDuplicate, 0, collectionOfSymbols);
				}
				toDuplicate = toDuplicate.Replace($"{toDuplicate[0]}", ""); // Удаляем символ, занесенный в массив символов.
			}
			string collectionOfSymbolsStr = collectionOfSymbols.ToString();
			char[] massiveOfSymbols = collectionOfSymbolsStr.ToCharArray();
			for (int index = 0; index < massiveOfSymbols.Length; index++)
			// Осуществляем дублирование символов, переданных в toDuplicate.
			{
				original = original.Replace(massiveOfSymbols[index].ToString(), massiveOfSymbols[index].ToString() + massiveOfSymbols[index].ToString());
			}
			return original;
		}
		private void AddDifferentRegister(string toDuplicate, int toDuplicateIndex, StringBuilder collectionOfSymbols)
        {
			if (char.IsLower(toDuplicate[toDuplicateIndex]))
			// Если теущий символ  принадлежит к нижнему регистру,то добавляем в массив этот символ с верхним регистром.
			{
				collectionOfSymbols.Append(char.ToUpper(toDuplicate[toDuplicateIndex]).ToString());
			}
			if (char.IsUpper(toDuplicate[toDuplicateIndex]))
			// Если теущий символ  принадлежит к верхнему регистру,то добавляем в массив этот символ с нижним регистром.
			{
				collectionOfSymbols.Append(char.ToLower(toDuplicate[toDuplicateIndex]).ToString());
			}
		}
	}
}