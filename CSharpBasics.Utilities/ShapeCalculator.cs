﻿using System;

namespace CSharpBasics.Utilities
{
	public class ShapeCalculator
	{
		//Реализовать метод, который вычисляет площадь прямоугольника со сторонами a и b.
		//Если пользователь передает некорректные значения (отрицательные или ноль), 
		//должно быть выброшено исключение ArgumentOutOfRangeException

		/// <summary>
		/// Возвращает площадь прямоугольника со сторонами <see cref="a"/> и <see cref="b"/>
		/// </summary>
		/// <param name="a">Длина стороны a прямоугольника</param>
		/// <param name="b">Длина стороны b прямоугольника</param>
		/// <returns>Площадь прямоугольника</returns>
		/// <exception cref="ArgumentOutOfRangeException"></exception>
		public float CalcRectangleArea(int a, int b)
		{
			if (a > 0 && b > 0)
			{
				return a * b;
			}
			else
            {
				if (a < 1)
				{
					throw new ArgumentOutOfRangeException(nameof(a), "Длина не может быть равна 0 или быть отрицательной");
				}
				throw new ArgumentOutOfRangeException(nameof(b), "Длина не может быть равна 0 или быть отрицательной");
				
			}
		}
	}
}