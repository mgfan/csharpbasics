﻿using System;
using System.Text;

namespace CSharpBasics.Utilities
{
	public class Calculator
	{
		/// <summary>
		/// Вычисляет сумму всех натуральных чисел меньше <see cref="number"/>, кратных любому из <see cref="divisors"/>
		/// </summary>
		/// <param name="number">Натуральное число</param>
		/// <param name="divisors">Числа, которым должны быть кратны слагаемые искомой суммы</param>
		/// <returns>Вычисленная сумма</returns>
		/// <exception cref="ArgumentOutOfRangeException">
		/// Выбрасывается в случае, если <see cref="number"/> или любое из чисел в <see cref="divisors"/> не является натуральным,
		/// а также если массив <see cref="divisors"/> пустой
		/// </exception>
		public float CalcSumOfDivisors(int number, params int[] divisors)
		{
			if (number > 1 && divisors.Length > 0)
			// Проверяем на натуральность число на 1 меньше number и есть ли делители.
			{
				return Summator(number, divisors);
			}
			if (number < 1)
			{
				throw new ArgumentOutOfRangeException(nameof(number), "Число не является натуральным");
			}
			else
			{
				throw new ArgumentOutOfRangeException(nameof(divisors), "Массив пуст");
			}
		}

		private float Summator(int number, params int[] divisors)
        {
			float sum = 0;  // Cумма чисел, кратных значениям.
			for (int i = divisors.Length - 1; i > -1; i--)
			{
				if (divisors[i] < 1)
				// Проверяем  на натуральность делители.
				{
					throw new ArgumentOutOfRangeException(nameof(divisors), "Делитель не натуральное число");
				}
				else
				{
					sum	= Sumer(divisors, number, i, sum);
				}
			}
			return sum;
		}

		private float Sumer(int[] divisors, int number, int index,float sum)
        {
			int a = number - 1;
			while (a > 0)
			//Проверяем больше ли делимое нуля, если меньше, то возвращаем делимое в начальное состояние.
			{
				if (a % divisors[index] == 0)
				//Проверяем есть ли остаток от деления, если нет, то прибавляем число к итоговому.
				{
					sum += a;
					a -= 1;
				}
				else
				{
					a -= 1;
				}
			}
			return sum;
		}

		/// <summary>
		/// Возвращает ближайшее наибольшее целое, состоящее из цифр исходного числа <see cref="number"/>
		/// </summary>
		/// <param name="number">Исходное число</param>
		/// <returns>Ближайшее наибольшее целое</returns>
		public long FindNextBiggerNumber(int number)
		{
			if (number < 1)
			{
				throw new ArgumentOutOfRangeException(nameof(number), "Число отрицательное или равно 0");
			}
			int[] numberToMassiv = Massiver(number);
			int indexNumberToMassiv = -1;

			// Проходимся по элементам введенного числа, начиная с конца и когда встречается первый случай,
			// Что теущий элемент больше предыдещего, записываем индекс предыдущего элемента для далнейшей работы.
			indexNumberToMassiv = FindIndex(numberToMassiv, indexNumberToMassiv);
			if (indexNumberToMassiv == -1)
			{
				// Если такой элемент числа не найден, значит поступившее число уже является минимальным значением
				// Поступившей комбинации чисел, в таком случае выводим исключение.
				throw new ArgumentOutOfRangeException(nameof(number), "Число уже является минимальным");
			}

			// Меняем месатами первое число с конца, которое оказалось меньше предыдущего на прердыдущее число
			// На то место, где раньше стояло меньшее число, ставим предыдущее и сортируем получившийся массив
			// Начиная с индекса числа, которое меньше.
			int reserv = numberToMassiv[indexNumberToMassiv];
			numberToMassiv[indexNumberToMassiv] = numberToMassiv[indexNumberToMassiv + 1];
			numberToMassiv[indexNumberToMassiv + 1] = reserv;
			Array.Sort(numberToMassiv, indexNumberToMassiv + 1, numberToMassiv.Length - indexNumberToMassiv - 1);
			return MassiveToLong(numberToMassiv);
		}

		static int[] Massiver(int number)
		{
			int[] numberToMassiv = new int[number.ToString().Length];
			int indexNumber = 0;
			string numberToString = number.ToString();
			foreach (char c in numberToString)
			{
				numberToMassiv[indexNumber] = c - '0';
				indexNumber++;
			}
			return numberToMassiv;
		}

		static int FindIndex(int[] numberToMassiv, int indexNumberToMassiv)
        {
			for (int i = numberToMassiv.Length - 1; i > 0; i--)
			{
				if (numberToMassiv[i] > numberToMassiv[i - 1])
				// Проходимся по элементам введенного числа, начиная с конца и когда встречается первый случай,
				// Что теущий элемент больше предыдещего, записываем индекс предыдущего элемента для далнейшей работы.
				{
					indexNumberToMassiv = (i - 1);
					break;
				}
			}
			return indexNumberToMassiv;
		}

		static long MassiveToLong (int[] numberToMassiv)
        {
			StringBuilder answerInStringBuild = new StringBuilder("");
			foreach (int i in numberToMassiv)
			{
				answerInStringBuild.Append(i.ToString());
			}
			string answerInString = answerInStringBuild.ToString();
			long answer = Int64.Parse(answerInString);
			return answer;
		}
	}
}