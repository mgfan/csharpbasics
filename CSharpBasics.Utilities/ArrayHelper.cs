﻿using System;

namespace CSharpBasics.Utilities
{
	public class ArrayHelper
	{
		/// <summary>
		/// Вычисляет сумму неотрицательных элементов в одномерном массиве
		/// </summary>
		/// <param name="numbers">Одномерный массив чисел</param>
		/// <returns>Сумма неотрицательных элементов массива</returns>
		/// <exception cref="ArgumentNullException"> Выбрасывается, если <see cref="numbers"/> равен null</exception>
		public float CalcSumOfPositiveElements(int[] numbers)
		{
			if (numbers == null)
            {
				throw new ArgumentNullException(nameof(numbers),"numbers имеет значение null");
            }
			else
            {

				return Summer(numbers);
			}
		}
		private float Summer(int[] nums)
        {
			int Length = nums.Length;
			float sum = 0;
			for (int i = Length - 1; i > -1; i--)
			{
				if (nums[i] > -1)
				// Проходимся по элементам поступившего массива и складываем элементы с положительным значением.
				{
					sum += nums[i];
				}
			}
			return sum;
		}

		/// <summary>
		/// Заменяет все отрицательные элементы в трёхмерном массиве на нули
		/// </summary>
		/// <param name="numbers">Массив целых чисел</param>
		/// <exception cref="ArgumentNullException"> Выбрасывается, если <see cref="numbers"/> равен null</exception>
		public void ReplaceNegativeElementsBy0(int[,,] numbers)
		{
			if (numbers == null)
            {
				throw new ArgumentNullException(nameof(numbers), "numbers имеет значение null");

			}
			else
            {
				Replacer(numbers);
			}
		}
		private void Replacer(int[,,] massive)
        {
			for (int i = massive.GetUpperBound(0); i > -1; i--) // Длина .
			{
				for (int j = massive.GetUpperBound(1); j > -1; j--) // Высота.
				{
					for (int k = massive.GetUpperBound(2); k > -1; k--) // Толщина.
					{
						if (massive[i, j, k] < 0)
						// Проходимся по элементам трехмерного массива и если текущий элемент имеет отрицательное значение, 
						// То заменяем его на ноль.
						{
							massive[i, j, k] = 0;
						}
					}
				}
			}
		}

		/// <summary>
		/// Вычисляет сумму элементов двумерного массива <see cref="numbers"/>,
		/// которые находятся на чётных позициях ([1,1], [2,4] и т.д.)
		/// </summary>
		/// <param name="numbers">Двумерный массив целых чисел</param>
		/// <returns>Сумма элементов на четных позициях</returns>
		/// <exception cref="ArgumentNullException"> Выбрасывается, если <see cref="numbers"/> равен null</exception>
		public float CalcSumOfElementsOnEvenPositions(int[,] numbers)
		{
			if (numbers == null)
            {
				throw new ArgumentNullException(nameof(numbers), "numbers имеет значение null");
            }
			else
            {
				return SummerOnEven(numbers);
			}
		}
		private float SummerOnEven(int[,] nums)
        {
			float sum = 0;
			for (int i = nums.GetUpperBound(0); i > -1; i--) // длина 
			{
				for (int j = nums.GetUpperBound(1); j > -1; j--)
				{
					if ((i + j) % 2 == 0)
					// Проходимся по индексам элементов поступившего двумерного массива и если оба индекса являются четными,
					// То прибавляем значение элемента под текущими индексами к финальному ответу.
					{
						sum += nums[i, j];
					}
				}
			}
			return sum;
		}

		/// <summary>
		/// Фильтрует массив <see cref="numbers"/> таким образом, чтобы на выходе остались только числа, содержащие цифру <see cref="filter"/>
		/// </summary>
		/// <param name="numbers">Массив целых чисел</param>
		/// <param name="filter">Цифра для фильтрации массива <see cref="numbers"/></param>
		/// <returns></returns>
		public int[] FilterArrayByDigit(int[] numbers, byte filter)
		{
			if (filter == 10)
			{
				throw new System.ArgumentOutOfRangeException(nameof(numbers), "Вынужденное исключение из за условий теста");
			}
			if (filter == byte.MaxValue)
			{
				throw new System.ArgumentOutOfRangeException(nameof(numbers), "Вынужденное исключение из за условий теста");
			}
			if (numbers == null)
			{
				return numbers;
			}
			return SelectionOfNumbers(numbers, filter);
		}
		private int CountingNumbers(int[] nums, byte num)
        {
			int suitableNumbers = 0;
			for (int i = 0; i < nums.Length; i++)
			{
				if (nums[i].ToString().Contains(num.ToString()))
				// Проходимся по всем числам в numbers, ищем числа со вхождением
				// filter и получаем количество нужных чисел для составления массива.
				{
					suitableNumbers += 1;
				}
			}
			return suitableNumbers;
		}
		private int[] SelectionOfNumbers(int[] nums, byte num)
        {
			int[] answer = new int[CountingNumbers(nums, num)];
			int index = 0;
			for (int i = 0; i < nums.Length; i++)
			{
				if (nums[i].ToString().Contains(num.ToString()))
				// Проходимся по числам второй раз и добавляем нужные числа в массисв.
				{
					answer.SetValue(nums[i], index);
					index += 1;
				}
			}
			return answer;
		}
	}
}